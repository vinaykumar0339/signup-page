const form = document.forms['form'];
const hideEye = document.getElementById('hide-eye');
const viewEye = document.getElementById('view-eye');
const confPassword = document.getElementById('confPassword');

// toggle the password visibility
hideEye.addEventListener('click', () => {

    let type = confPassword.getAttribute('type');
    type = type === 'password' ? 'text' : 'password';
    confPassword.setAttribute('type', type);
    hideEye.classList.toggle('hide');
    viewEye.classList.toggle('hide');
})

// toggle the password hidden
viewEye.addEventListener('click', () => {

    let type = confPassword.getAttribute('type');
    type = type === 'text' ? 'password' : 'text';
    confPassword.setAttribute('type', type);
    hideEye.classList.toggle('hide');
    viewEye.classList.toggle('hide');
})

// validate form
function handleSubmit() {
    const userError = document.querySelector('#user-error');
    const emailError = document.querySelector('#email-error');
    const passwordError = document.querySelector('#password-error');
    const confPasswordError = document.querySelector('#confPassword-error');
    let isError = false;

    // email regex
    const emailMatch = /^([a-z\d\.-]+)@([a-z\d-]+)\.([a-z]{2,8})(\.[a-z]{2,8})?$/;

    const username = form['username'].value.trim();
    const email = form['email'].value.trim();
    const password = form['password'].value.trim();
    const confPassword = form['confPassword'].value.trim();

    if (username.length < 6) {
        isError = true;
        displayErrors('username', userError, 'username required minimum 6 characters');
    } else if (username.includes(' ')) {
        isError = true;
        displayErrors('username', userError, 'remove spaces between the username');
    } else {
        hideErrors('username', userError);
    }

    if (!emailMatch.test(email)) {
        isError = true;
        displayErrors('email', emailError, 'please enter valid email address');
    } else {
        hideErrors('email', emailError);

    }

    if (password.length < 6) {
        isError = true;
        displayErrors('password', passwordError, 'password required minimum 6 characters');
    } else {
        hideErrors('password', passwordError);
    }

    if (password !== confPassword) {
        isError = true;
        displayErrors('confPassword', confPasswordError, 'password not matched');
    } else {
        hideErrors('confPassword', confPasswordError);

    }

    return !isError;
}

// set the errors
function displayErrors(fieldName, errorTag, errorMsg) {
    form[fieldName].classList.remove('success');
    form[fieldName].classList.add('error');

    errorTag.classList.add('error');
    errorTag.innerText = errorMsg;
}

// remove the errors
function hideErrors(fieldName, errorTag) {
    form[fieldName].classList.remove('error');
    form[fieldName].classList.add('success');
    errorTag.classList.remove('error')
}